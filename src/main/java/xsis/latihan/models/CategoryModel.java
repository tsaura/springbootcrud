package xsis.latihan.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name="category")
public class CategoryModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long CategoryId;
	
	@Column(name="name")
	private String Name;
	
	
	public Long getCategoryId() {
		return CategoryId;
	}

	public void setCategoryId(Long categoryid) {
		CategoryId = categoryid;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}
	
	public CategoryModel() {
		
	}
	public CategoryModel(String name) {
		this.Name=name;
	}
	
}
