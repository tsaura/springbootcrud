package xsis.latihan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.repositories.CategoryRepo;
import xsis.latihan.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService{
	@Autowired
	private CategoryRepo categoryrepo;
	
	@Override
	public List<CategoryModel> findAllCategory(){
		return categoryrepo.findAll();
	}

	@Override
	public CategoryModel save(CategoryModel category) {
		// TODO Auto-generated method stub
		return categoryrepo.save(category);
	}

	@Override
	public void delete(Long CategoryId) {
		categoryrepo.deleteById(CategoryId);
		// TODO Auto-generated method stub
	}
}
